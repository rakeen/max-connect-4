"""
Shadman Rakeen
123 1168 642
Assignment#2
"""

from copy import *
from operator import itemgetter
from game_util import * 



def cntConsec(Board):
	p1=[0]*8 # p1[2] means the count of consecutive 2 pieces
	p2=[0]*8

	# Horizontal
	for row in Board:
		for i in range(6):
			for k in range(2,8): # consec piece
				if i+k>7: break
				if row[i:i+k]==[1]*k:
					p1[k]+=1
				elif row[i:i+k]==[2]*k:
					p2[k]+=1

	# Vertical
	for col in range(7):
		for row in range(6):
			for k in range(2,7):
				if row+k>6: break

				no=True
				for consec in range(0,k):
					if Board[row+consec][col]!=1:
						no=False
						break
				if no==True:
					p1[k]+=1

				no=True
				for consec in range(0,k):
					if Board[row+consec][col]!=2:
						no=False
						break
				if no==True:
					p2[k]+=1

	# Diagonal-Forward
	for i in range(6):
		for j in range(7):
			for consec in range(2,7):
				if i-consec<0 or j+consec>6: break

				no=True
				for k in range(0,consec):
					if Board[i-k][j+k]!=1:
						no=False
						break
				if no==True:
					#print k
					p1[consec]+=1

				no=True
				for k in range(0,consec):
					if Board[i-k][j+k]!=2:
						no=False
						break
				if no==True:
					p2[consec]+=1

	# Diagonal-Backward
	for i in range(6):
		for j in range(7):
			for consec in range(2,7):
				if i-consec<0 or j-consec<0: break

				no=True
				for k in range(0,consec):
					if Board[i-k][j-k]!=1:
						no=False
						break
				if no==True:
					p1[consec]+=1

				no=True
				for k in range(0,consec):
					if Board[i-k][j-k]!=2:
						no=False
						break
				if no==True:
					p2[consec]+=1


	return (p1,p2)



def eval(Board,player):
	p1,p2=cntConsec(Board)

	if player==1:
		#print "p1 eval:",(p1[2]-p2[2]),5*(p1[3]-p2[3])
		return 15*(p1[5]-p2[5])+10*(p1[4]-p2[4])+5*(p1[3]-p2[3])+(p1[2]-p2[2])
	if player==2:
		#print "p2 eval:",p2[2],p1[2]
		return 15*(p1[5]-p2[5])+10*(p2[4]-p1[4])+5*(p2[3]-p1[3])+(p2[2]-p1[2])


def maxf(Board,depth,player,a,b):
	if depth==0 or isFull(Board)==True:
		return (eval(Board,player),0)

	bestVal=-100000000
	pos=0

	order=[3,2,4,1,5,0,6]
	for i in order:
		newB=playMove(Board,i,player)
		if newB!=-1:
			val,dummy=minf(newB,depth-1,switchPlayer(player),a,b)
			#print "maxf: depth~",depth," player~",player," Val~\t",val," pos~",i
			if bestVal<val:

				bestVal=val
				pos=i
				#max
				if a<bestVal:
					a=bestVal
				
				#pruning
				if b<=a:
					#print a,b
					#return (a,pos) # beta-cut
					break
				

	#print "maxf: depth~",depth," player~",player," bestVal~",bestVal," pos~",pos
	return (bestVal,pos)

def minf(Board,depth,player,a,b):
	if depth==0 or isFull(Board)==True:
		
		'''
		print "inside minf:\n",printBoard(Board)
		print "eval: ",eval(Board,player),"\t player# ",player
		'''
		return (-eval(Board,player),0) # <=magic

	bestVal=100000000
	pos=0

	order=[3,2,4,1,5,0,6]
	for i in order:
		newB=playMove(Board,i,player)

		if newB!=-1:
			
			val,dummy=maxf(newB,depth-1,switchPlayer(player),a,b)
			#print "minf: depth~",depth," player~",player," val~\t",val," pos~",i
			if bestVal>val:
				bestVal=val			
				pos=i
				#min
				if b>=bestVal:
					b=bestVal 
				
				#pruning
				if b<=a:
					#print a,b
					#return (b,pos)
					break
				
	#print "after minf:",bestVal
	#print "minf: depth~",depth," player~",player," bestVal~",bestVal," pos~",pos
	return (bestVal,pos)


def alpha_beta_pruning(Board,depth,player):
	val,pos=maxf(Board,depth,player,-100000000,100000000)
	return pos

def nextMove_AI(Board,depth,player):
	return alpha_beta_pruning(Board,depth,player)



def iterativeDeepening(Board,depth,player):
	ans=-1
	for i in range(1,depth+1):
		mv = nextMove_AI(Board,i,player)
		ans=mv
		if playMove(Board,mv,player)==-1: return ans
	return ans
