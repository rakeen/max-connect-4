"""
Shadman Rakeen
123 1168 642
Assignment#2
"""

from copy import * 

def switchPlayer(p):
	#print "inside switchPlayer func:\t",p
	if p==1: return 2
	elif p==2: return 1

def printBoard(Board):
	print ' -----------------'
	for i in range(6):
		print ' |',
		for j in range(7):
			print Board[i][j],
		print '| '
	print ' -----------------'



# ref: http://stackoverflow.com/a/1009764
def printToFile(Board,outfile):
	with open(outfile, 'w') as file:
		file.writelines(''.join(str(j) for j in i) + '\n' for i in Board)


# col= 0-indexed
def playMove(B,col,player):
	Board=deepcopy(B)
	if Board[0][col]!=0: return -1
	
	idx=0
	while Board[idx][col]==0:
		idx+=1
		if idx==6: break

	#print idx,col
	#printBoard(Board)
	Board[idx-1][col]=player
	return Board

def isGameOver(Board):
	for i in range(6):
		for j in range(7):
			if Board[i][j]==0: return False 
	return True


def isFull(Board):
	for i in Board:
		for j in i:
			if j==0: return False
	return True

def p1_move(Board):
	def valid(n):
		if n<0 or n>6: return False
		if Board[0][n]!=0: return False 
		return True

	move = int(raw_input("Your move...")) # 1-indexed
	move-=1

	while valid(move)==False:
		move = int(raw_input("Your move...")) # 1-indexed
		move-=1
	return move




# Calculate the number of 4-in-a-row each player has
def countScore(Board):
	p1Score = 0;
	p2Score = 0;

	# Check horizontally
	for row in Board:
		# Check player 1
		if row[0:4] == [1]*4:
			p1Score += 1
		if row[1:5] == [1]*4:
			p1Score += 1
		if row[2:6] == [1]*4:
			p1Score += 1
		if row[3:7] == [1]*4:
			p1Score += 1
		# Check player 2
		if row[0:4] == [2]*4:
			p2Score += 1
		if row[1:5] == [2]*4:
			p2Score += 1
		if row[2:6] == [2]*4:
			p2Score += 1
		if row[3:7] == [2]*4:
			p2Score += 1

	# Check vertically
	for j in range(7):
		# Check player 1
		if (Board[0][j] == 1 and Board[1][j] == 1 and
			   Board[2][j] == 1 and Board[3][j] == 1):
			p1Score += 1
		if (Board[1][j] == 1 and Board[2][j] == 1 and
			   Board[3][j] == 1 and Board[4][j] == 1):
			p1Score += 1
		if (Board[2][j] == 1 and Board[3][j] == 1 and
			   Board[4][j] == 1 and Board[5][j] == 1):
			p1Score += 1
		# Check player 2
		if (Board[0][j] == 2 and Board[1][j] == 2 and
			   Board[2][j] == 2 and Board[3][j] == 2):
			p2Score += 1
		if (Board[1][j] == 2 and Board[2][j] == 2 and
			   Board[3][j] == 2 and Board[4][j] == 2):
			p2Score += 1
		if (Board[2][j] == 2 and Board[3][j] == 2 and
			   Board[4][j] == 2 and Board[5][j] == 2):
			p2Score += 1

	# Check diagonally

	# Check player 1
	if (Board[2][0] == 1 and Board[3][1] == 1 and
		   Board[4][2] == 1 and Board[5][3] == 1):
		p1Score += 1
	if (Board[1][0] == 1 and Board[2][1] == 1 and
		   Board[3][2] == 1 and Board[4][3] == 1):
		p1Score += 1
	if (Board[2][1] == 1 and Board[3][2] == 1 and
		   Board[4][3] == 1 and Board[5][4] == 1):
		p1Score += 1
	if (Board[0][0] == 1 and Board[1][1] == 1 and
		   Board[2][2] == 1 and Board[3][3] == 1):
		p1Score += 1
	if (Board[1][1] == 1 and Board[2][2] == 1 and
		   Board[3][3] == 1 and Board[4][4] == 1):
		p1Score += 1
	if (Board[2][2] == 1 and Board[3][3] == 1 and
		   Board[4][4] == 1 and Board[5][5] == 1):
		p1Score += 1
	if (Board[0][1] == 1 and Board[1][2] == 1 and
		   Board[2][3] == 1 and Board[3][4] == 1):
		p1Score += 1
	if (Board[1][2] == 1 and Board[2][3] == 1 and
		   Board[3][4] == 1 and Board[4][5] == 1):
		p1Score += 1
	if (Board[2][3] == 1 and Board[3][4] == 1 and
		   Board[4][5] == 1 and Board[5][6] == 1):
		p1Score += 1
	if (Board[0][2] == 1 and Board[1][3] == 1 and
		   Board[2][4] == 1 and Board[3][5] == 1):
		p1Score += 1
	if (Board[1][3] == 1 and Board[2][4] == 1 and
		   Board[3][5] == 1 and Board[4][6] == 1):
		p1Score += 1
	if (Board[0][3] == 1 and Board[1][4] == 1 and
		   Board[2][5] == 1 and Board[3][6] == 1):
		p1Score += 1

	if (Board[0][3] == 1 and Board[1][2] == 1 and
		   Board[2][1] == 1 and Board[3][0] == 1):
		p1Score += 1
	if (Board[0][4] == 1 and Board[1][3] == 1 and
		   Board[2][2] == 1 and Board[3][1] == 1):
		p1Score += 1
	if (Board[1][3] == 1 and Board[2][2] == 1 and
		   Board[3][1] == 1 and Board[4][0] == 1):
		p1Score += 1
	if (Board[0][5] == 1 and Board[1][4] == 1 and
		   Board[2][3] == 1 and Board[3][2] == 1):
		p1Score += 1
	if (Board[1][4] == 1 and Board[2][3] == 1 and
		   Board[3][2] == 1 and Board[4][1] == 1):
		p1Score += 1
	if (Board[2][3] == 1 and Board[3][2] == 1 and
		   Board[4][1] == 1 and Board[5][0] == 1):
		p1Score += 1
	if (Board[0][6] == 1 and Board[1][5] == 1 and
		   Board[2][4] == 1 and Board[3][3] == 1):
		p1Score += 1
	if (Board[1][5] == 1 and Board[2][4] == 1 and
		   Board[3][3] == 1 and Board[4][2] == 1):
		p1Score += 1
	if (Board[2][4] == 1 and Board[3][3] == 1 and
		   Board[4][2] == 1 and Board[5][1] == 1):
		p1Score += 1
	if (Board[1][6] == 1 and Board[2][5] == 1 and
		   Board[3][4] == 1 and Board[4][3] == 1):
		p1Score += 1
	if (Board[2][5] == 1 and Board[3][4] == 1 and
		   Board[4][3] == 1 and Board[5][2] == 1):
		p1Score += 1
	if (Board[2][6] == 1 and Board[3][5] == 1 and
		   Board[4][4] == 1 and Board[5][3] == 1):
		p1Score += 1

	# Check player 2
	if (Board[2][0] == 2 and Board[3][1] == 2 and
		   Board[4][2] == 2 and Board[5][3] == 2):
		p2Score += 1
	if (Board[1][0] == 2 and Board[2][1] == 2 and
		   Board[3][2] == 2 and Board[4][3] == 2):
		p2Score += 1
	if (Board[2][1] == 2 and Board[3][2] == 2 and
		   Board[4][3] == 2 and Board[5][4] == 2):
		p2Score += 1
	if (Board[0][0] == 2 and Board[1][1] == 2 and
		   Board[2][2] == 2 and Board[3][3] == 2):
		p2Score += 1
	if (Board[1][1] == 2 and Board[2][2] == 2 and
		   Board[3][3] == 2 and Board[4][4] == 2):
		p2Score += 1
	if (Board[2][2] == 2 and Board[3][3] == 2 and
		   Board[4][4] == 2 and Board[5][5] == 2):
		p2Score += 1
	if (Board[0][1] == 2 and Board[1][2] == 2 and
		   Board[2][3] == 2 and Board[3][4] == 2):
		p2Score += 1
	if (Board[1][2] == 2 and Board[2][3] == 2 and
		   Board[3][4] == 2 and Board[4][5] == 2):
		p2Score += 1
	if (Board[2][3] == 2 and Board[3][4] == 2 and
		   Board[4][5] == 2 and Board[5][6] == 2):
		p2Score += 1
	if (Board[0][2] == 2 and Board[1][3] == 2 and
		   Board[2][4] == 2 and Board[3][5] == 2):
		p2Score += 1
	if (Board[1][3] == 2 and Board[2][4] == 2 and
		   Board[3][5] == 2 and Board[4][6] == 2):
		p2Score += 1
	if (Board[0][3] == 2 and Board[1][4] == 2 and
		   Board[2][5] == 2 and Board[3][6] == 2):
		p2Score += 1

	if (Board[0][3] == 2 and Board[1][2] == 2 and
		   Board[2][1] == 2 and Board[3][0] == 2):
		p2Score += 1
	if (Board[0][4] == 2 and Board[1][3] == 2 and
		   Board[2][2] == 2 and Board[3][1] == 2):
		p2Score += 1
	if (Board[1][3] == 2 and Board[2][2] == 2 and
		   Board[3][1] == 2 and Board[4][0] == 2):
		p2Score += 1
	if (Board[0][5] == 2 and Board[1][4] == 2 and
		   Board[2][3] == 2 and Board[3][2] == 2):
		p2Score += 1
	if (Board[1][4] == 2 and Board[2][3] == 2 and
		   Board[3][2] == 2 and Board[4][1] == 2):
		p2Score += 1
	if (Board[2][3] == 2 and Board[3][2] == 2 and
		   Board[4][1] == 2 and Board[5][0] == 2):
		p2Score += 1
	if (Board[0][6] == 2 and Board[1][5] == 2 and
		   Board[2][4] == 2 and Board[3][3] == 2):
		p2Score += 1
	if (Board[1][5] == 2 and Board[2][4] == 2 and
		   Board[3][3] == 2 and Board[4][2] == 2):
		p2Score += 1
	if (Board[2][4] == 2 and Board[3][3] == 2 and
		   Board[4][2] == 2 and Board[5][1] == 2):
		p2Score += 1
	if (Board[1][6] == 2 and Board[2][5] == 2 and
		   Board[3][4] == 2 and Board[4][3] == 2):
		p2Score += 1
	if (Board[2][5] == 2 and Board[3][4] == 2 and
		   Board[4][3] == 2 and Board[5][2] == 2):
		p2Score += 1
	if (Board[2][6] == 2 and Board[3][5] == 2 and
		   Board[4][4] == 2 and Board[5][3] == 2):
		p2Score += 1

	return (p1Score,p2Score)