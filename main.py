"""
Shadman Rakeen
123 1168 642
Assignment#2
"""

import os.path	# for file checking
from game_util import *
from misc_util import *
from ai import *


prompt()


Board = None

if sys.argv[1]=="one-move":
	nextPiece,Board = readFile(sys.argv[2])
	depth=int(sys.argv[4])
	player=nextPiece

	#p1,p2 = cntConsec(Board)
	#print p1
	
	if isGameOver(Board)==False:
		ai_move = nextMove_AI(Board,depth,player) 
		#ai_move = iterativeDeepening(Board,depth,player) 
		Board = playMove(Board,ai_move,player) 
	else: print "Game Over!" # not specified in the problem statement

	
	printBoard(Board)
	p1,p2=countScore(Board)
	print "Player1 Score:",p1,"\tPlayer2 Score:",p2,"\n" # player1 means one who plays the 1-piece

	Board.append([switchPlayer(player)]) # next player
	printToFile(Board,sys.argv[3])
	

elif sys.argv[1]=="interactive":

	# Creates an empty Board if file doesn't exist
	if os.path.exists(sys.argv[2])==False:
		Board=[[0 for x in range(7)] for x in range(6)]		#ref:http://stackoverflow.com/a/6667288
		Board.append([1])
		printToFile(Board,sys.argv[2])

	nextPiece,B = readFile(sys.argv[2])
	Board = deepcopy(B)
	player=nextPiece
	depth=int(sys.argv[4])


	human=None
	if sys.argv[3]=="human-next": human=player
	else: human=switchPlayer(player)
	

	printBoard(Board)
	p1,p2=countScore(Board)
	print "Player1 Score:",p1,"\tPlayer2 Score:",p2,"\n"


	while isGameOver(Board)==False:
		#human
		if human==player:
			print ""
			p1=p1_move(Board)
			Board = playMove(Board,p1,player)

			boardLog=deepcopy(Board)
			boardLog.append([switchPlayer(player)]) # next player
			printToFile(boardLog,"human.txt")
		#ai
		else:
			ai_move = nextMove_AI(Board,depth,player)
			print "\nAI played column#:",ai_move+1
			Board = playMove(Board,ai_move,player)
			if Board==-1: print >>sys.stderr, "oops! something went wrong...\nAI played a move that is invalid"

			boardLog=deepcopy(Board)
			boardLog.append([switchPlayer(player)]) # next player
			printToFile(boardLog,"computer.txt")
			
		printBoard(Board)
		p1,p2=countScore(Board)
		print "Player1 Score:",p1,"\tPlayer2 Score:",p2 # player1 means one who plays the 1-piece
		player=switchPlayer(player)

	print "================"
	print "Game Over!"
	if p1>p2: print "Player-1 win!" # player1 means one who plays the 1-piece
	elif p1<p2: print "Player-2 win!"
	else: print "Draw!"