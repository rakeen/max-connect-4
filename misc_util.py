"""
Shadman Rakeen
123 1168 642
Assignment#2
"""

import sys

def argument_check_interactive():
	return ~(sys.argv[3]=="computer-next" or sys.argv[3]=="human-text")
		

'''
argv list:
0 - file it self!
1 - interactive | one-move
2 - input.txt
3 - computer-next | output.txt 
4 - depth
'''
# todo: man page
def prompt():
	while len(sys.argv)!=5:
		print "Please provide correct arguments(there should be 4 arguments!):"
		sys.argv=raw_input().split()

	if sys.argv[1]=="interactive":
		while argument_check_interactive()==1:
			print "Please provide correct arguments(3rd argument should be one of 'computer-next' or 'human-next'!): "
			sys.argv=raw_input().split()
    #print >> sys.stderr, sys.argv[0],sys.argv[1],sys.argv[2]


def readFile(filename):
	B = [[0 for i in range(7)] for j in range(6)]
	nextPiece=None
	with open(filename,"r") as F:
		temp =  [line.strip() for line in F]
		for idx,i in enumerate(temp):
			if len(i)==1: nextPiece=int(i[0])
			else: B[idx]=[ int(j.strip()) for j in i]
			#print B[idx]
	return (nextPiece,B)

