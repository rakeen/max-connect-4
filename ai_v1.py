from game_util import * 
from copy import *
from operator import itemgetter


def count2s(Board):
		player12=0
		player22=0

		for row in Board:
			# Check player 1
			if row[0:2] == [1]*2:
				player12 += 1
			if row[1:3] == [1]*2:
				player12 += 1
			if row[2:4] == [1]*2:
				player12 += 1
			if row[3:5] == [1]*2:
				player12 += 1
			# Check player 2
			if row[0:2] == [2]*2:
				player22 += 1
			if row[1:3] == [2]*2:
				player22 += 1
			if row[2:4] == [2]*2:
				player22 += 1
			if row[3:5] == [2]*2:
				player22 += 1

        # Check vertically
		for j in range(7):
			# Check player 1
			if (Board[0][j] == 1 and Board[1][j] == 1):
				player12 += 1
			if (Board[1][j] == 1 and Board[2][j] == 1):
				player12 += 1
			if (Board[2][j] == 1 and Board[3][j] == 1):
				player12 += 1
			# Check player 2
			if (Board[0][j] == 2 and Board[1][j] == 2):
				player22 += 1
			if (Board[1][j] == 2 and Board[2][j] == 2):
				player22 += 1
			if (Board[2][j] == 2 and Board[3][j] == 2):
				player22 += 1

		return (player12,player22)


def count3s(Board):
	player13=0
	player23=0
        
	for row in Board:
		# Check player 1
		if row[0:3] == [1]*3:
			player13 += 1
		if row[1:4] == [1]*3:
			player13 += 1
		if row[2:5] == [1]*3:
			player13 += 1
		if row[3:6] == [1]*3:
			player13 += 1
		# Check player 2
		if row[0:3] == [2]*3:
			player23 += 1
		if row[1:4] == [2]*3:
			player23 += 1
		if row[2:5] == [2]*3:
			player23 += 1
		if row[3:6] == [2]*3:
			player23 += 1

	# Check vertically
	for j in range(7):
		# Check player 1	
		if (Board[0][j] == 1 and Board[1][j] == 1 and Board[2][j] == 1):
			player13 += 1
		if (Board[1][j] == 1 and Board[2][j] == 1 and Board[3][j] == 1):
			player13 += 1
		if (Board[2][j] == 1 and Board[3][j] == 1 and Board[4][j] == 1):
			player13 += 1
		# Check player 2
		if (Board[0][j] == 2 and Board[1][j] == 2 and Board[2][j] == 2):
			player23 += 1
		if (Board[1][j] == 2 and Board[2][j] == 2 and Board[3][j] == 2):
			player23 += 1
		if (Board[2][j] == 2 and Board[3][j] == 2 and Board[4][j] == 2):
			player23 += 1
	
	return (player13,player23)



def eval(Board,player):
	_3_p1,_3_p2=count3s(Board)
	_2_p1,_2_p2=count2s(Board)

	if player==1:
		print "p1",50*(_3_p1-_3_p2)+2*(_2_p1-_2_p2)
		return 50*(_3_p1-_3_p2)+2*(_2_p1-_2_p2)
	if player==2:
		print "p2",5*(_3_p2-_3_p1)+2*(_2_p2-_2_p1)
		return 5*(_3_p2-_3_p1)+2*(_2_p2-_2_p1)


def maxf(Board,depth,player,a,b):
	if depth==0: return eval(Board,player)
	val=-100000000
	for i in range(7):
		newB=playMove(Board,i,player)
		if newB!=-1:
			val=max(val,minf(newB,depth-1,switchPlayer(player),a,b))
			if val>=b: return val
			a=max(a,val) 
	return val

def minf(Board,depth,player,a,b):
	if depth==0: return eval(Board,player)

	values=[]
	val=100000000
	for i in range(7):
		newB=playMove(Board,i,player)
		if newB!=-1:
			val=min(val,maxf(newB,depth-1,switchPlayer(player),a,b))
			if val<=a: return val
			b=min(b,val)
	return val


def alpha_beta_pruning(Board,depth,player):
	values=[]
	val=-100000000
	for i in range(7):
		newB = playMove(Board,i,player)
		if newB==-1: continue
		val=max(val,minf(newB,depth-1,switchPlayer(player),-100000000,100000000))

		#ref: http://stackoverflow.com/questions/13145368/find-the-maximum-value-in-a-list-of-tuples-in-python
		#ref: http://stackoverflow.com/questions/4800419/sorting-or-finding-max-value-by-the-second-element-in-a-nested-list-python
		values.append((val,i))
		optimalVal=max(values,key=itemgetter(1))[1]
		#optimalIndex=values.index(optimalVal)
		
	#print optimalVal
	return optimalVal

def nextMove_AI(Board,depth,player):
	return alpha_beta_pruning(Board,depth,player)