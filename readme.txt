This was an assignment for Artificial Intelligence course(CSE440) taught in NSU.  
pset: (http://ece.northsouth.edu/~lutfe.elahi/2015Fall/CSE440/hw/A2/CSE440_A2.pdf)[http://ece.northsouth.edu/~lutfe.elahi/2015Fall/CSE440/hw/A2/CSE440_A2.pdf]


##Note:

- Evaluation Function: 15*(p1[5]-p2[5])+10*(p1[4]-p2[4])+5*(p1[3]-p2[3])+(p1[2]-p2[2])  
where,  
	p1[i] = number of consecutive(vertically,horizontally or diagonally) i '1's  
	p2[i] = number of consecutive(vertically,horizontally or diagonally) i '2's

- Iterative deepening isn't useful in this case, since we won't find the actual optimal solution before depth 42(in worst case).  
So, running minimax at each level is worthless, because all the possible actual goal lies in the same depth.  
But just to demonstrate, what iterativeDeepening() inside ai.py does is, it runs the minimax function at each level upto our given depth(in the input).  
If it finds a situation where the user/computer can't make a valid move in any column, then it returns the answer.  
- Pruning is done inside the minf and maxf function. Making the pruning was worthit as it reduces the runtime to almost half!!  
- Pruning is optimal when we traverse the nodes in optimal order. In classic connect-4(where the game stops if any player matches 4 pieces) game the "odd/even threat" plays a significant role. So, propagating from the middle reduced the runtime even more!  


##Instruction:

- Run main.py with the stated(in the problem statement) arguments. i.e
	+ python main.py interactive in.txt computer-next 3
	+ python main.py one-move in.txt out.txt 3
- Note that in "one-move" mode the input file must be accessible.
- Tested in python2.7


##Todo:

[ ] Improve the eval func  
[ ] Consider some odd-even threats tactics  
[ ] Use caching(dataset)  
[ ] Use nural network